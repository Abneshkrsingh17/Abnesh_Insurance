package com.emidhealth.insurance.HealthInsurance.Entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="Customer_Details")
public class CustomerDetails { 
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="name")
	private String custName;
	
	@Column(name="age")
	private int age;
	
	@Column(name="gender")
	private String gender;
	
	
	@Column(name="Hypertension")
	private String Hypertension;
	
	@Column(name="bloodPressure")
	private String bloodPressure;
	
	@Column(name="bloodSugar")
	private String bloodSugar;
	
	@Column(name="overWeight")
	private String overWeight;
	
	@Column(name="smoking")
	private String smoking;
	
	@Column(name="alcohal")
	private String alcohal;
	
	@Column(name="dailyEx")
	private String dailyEx;
	
	@Column(name="drugs")
	private String drugs;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getHypertension() {
		return Hypertension;
	}

	public void setHypertension(String hypertension) {
		Hypertension = hypertension;
	}

	public String getBloodPressure() {
		return bloodPressure;
	}

	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	public String getBloodSugar() {
		return bloodSugar;
	}

	public void setBloodSugar(String bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	public String getOverWeight() {
		return overWeight;
	}

	public void setOverWeight(String overWeight) {
		this.overWeight = overWeight;
	}

	public String getSmoking() {
		return smoking;
	}

	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}

	public String getAlcohal() {
		return alcohal;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setAlcohal(String alcohal) {
		this.alcohal = alcohal;
	}

	public String getDailyEx() {
		return dailyEx;
	}

	public void setDailyEx(String dailyEx) {
		this.dailyEx = dailyEx;
	}

	public String getDrugs() {
		return drugs;
	}

	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}

	

	
	
	
	
	

}
