package com.emidhealth.insurance.HealthInsurance.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emidhealth.insurance.HealthInsurance.Entity.CustomerDetails;
import com.emidhealth.insurance.HealthInsurance.dto.CustomerHealthHabitDto;
import com.emidhealth.insurance.HealthInsurance.service.PremiumService;

@Controller
@RequestMapping(value="/getInsurancePremium")
public class InsuranceController {
	
	@Autowired 
	PremiumService service;
	
	@RequestMapping(value = "/premium", method = RequestMethod.GET)
	public String getPremiumAmount(@ModelAttribute("customerDetails") CustomerDetails customerDetails, Model model) {
		
		model.addAttribute("listPersons", this.service.getPremiumAmmount(customerDetails));
		return "premium";
		
		
		//return "";
	}

}
