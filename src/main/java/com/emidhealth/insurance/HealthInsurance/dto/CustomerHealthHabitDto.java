package com.emidhealth.insurance.HealthInsurance.dto;

public class CustomerHealthHabitDto {
	
	private String name; 
	private String gender;
	private int age;
	private String hypertnsn;
	private String bp;
	private String bldSgr;
	private String ovrWt;
	
	private String smoking;
	private String alcoal;
	private String dailyEx;
	private String drugs;
	
	public CustomerHealthHabitDto(String name, String gender, String hypertnsn, String bp, String bs, String ovrwt, String smoking,
			String alcohal, String dailyEx, String drugs, int age ) {
		
		this.name=name;
		this.gender=gender;
		this.hypertnsn=hypertnsn;
		this.bp=bp;
		this.bldSgr=bs;
		this.ovrWt=ovrwt;
		this.smoking=smoking;
		this.alcoal=alcohal;
		this.dailyEx=dailyEx;
		this.drugs=drugs;
		this.age=age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getHypertnsn() {
		return hypertnsn;
	}
	public void setHypertnsn(String hypertnsn) {
		this.hypertnsn = hypertnsn;
	}
	public String getBp() {
		return bp;
	}
	public void setBp(String bp) {
		this.bp = bp;
	}
	public String getBldSgr() {
		return bldSgr;
	}
	public void setBldSgr(String bldSgr) {
		this.bldSgr = bldSgr;
	}
	public String getOvrWt() {
		return ovrWt;
	}
	public void setOvrWt(String ovrWt) {
		this.ovrWt = ovrWt;
	}
	public String getSmoking() {
		return smoking;
	}
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	public String getAlcoal() {
		return alcoal;
	}
	public void setAlcoal(String alcoal) {
		this.alcoal = alcoal;
	}
	public String getDailyEx() {
		return dailyEx;
	}
	public void setDailyEx(String dailyEx) {
		this.dailyEx = dailyEx;
	}
	public String getDrugs() {
		return drugs;
	}
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}
	
	

}
