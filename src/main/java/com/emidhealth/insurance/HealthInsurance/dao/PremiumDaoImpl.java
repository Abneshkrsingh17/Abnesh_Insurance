package com.emidhealth.insurance.HealthInsurance.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.emidhealth.insurance.HealthInsurance.Entity.CustomerDetails;

public class PremiumDaoImpl implements PremiumDao {

	@PersistenceContext 
	EntityManager em;
	
	public float getCustomerPremiumDetails(CustomerDetails customerDetails ) {
		int age=customerDetails.getAge();
		String gender=customerDetails.getGender();
		float premiumAmount=5000;
		
		if(age<18) {
			return premiumAmount;
		}else if(age>=18 ) {
			//if(gender.equals("M") || gender.equals("F")) {
				if(age>=18 && age<=25)
					premiumAmount=premiumAmount+((premiumAmount*10)/100);
				else if(age>25 && age<=30)
					premiumAmount=premiumAmount+((premiumAmount*20)/100);
				else if(age>30 && age<=35)
					premiumAmount=premiumAmount+((premiumAmount*30)/100);
				else if(age>35 && age<=40)
					premiumAmount=premiumAmount+((premiumAmount*40)/100);
				else
					premiumAmount=premiumAmount+((premiumAmount*60)/100);
				//
				if(gender.equals("M"))
					premiumAmount=premiumAmount+((premiumAmount*2)/100);
					
				if(customerDetails.getBloodSugar().equals("Y")|| customerDetails.getBloodPressure().equals("Y")|| customerDetails.getHypertension().equals("Y")) {
					premiumAmount=premiumAmount+((premiumAmount*1)/100);
				}
				
				if(customerDetails.getDailyEx().equals("Y")) {
					premiumAmount=premiumAmount-((premiumAmount*3)/100);
				}
				
				if(customerDetails.getAlcohal().equals("Y")||customerDetails.getDrugs().equals("Y")||customerDetails.getSmoking().equals("Y")) {
					premiumAmount=premiumAmount+((premiumAmount*3)/100);
				}
				return premiumAmount;
		}
		return 0;
	}
	/*public static void main(String[] args) {
		
		CustomerHealthHabitcustomerDetails chh=new CustomerHealthHabitcustomerDetails("Abnesh", "M", "N", "N", "N", "N", "N",
			"Y", "Y", "N", 34 );
		
		System.out.println(new PremiumDaoImpl().getCustomerPremiumDetails(chh));
		
	}*/

}
