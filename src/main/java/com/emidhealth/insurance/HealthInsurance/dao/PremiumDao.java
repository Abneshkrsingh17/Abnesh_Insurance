package com.emidhealth.insurance.HealthInsurance.dao;

import com.emidhealth.insurance.HealthInsurance.Entity.CustomerDetails;

public interface PremiumDao { 

	public float getCustomerPremiumDetails(CustomerDetails customerDetails);
}
