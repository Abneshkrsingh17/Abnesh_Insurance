package com.emidhealth.insurance.HealthInsurance.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.emidhealth.insurance.HealthInsurance.Entity.CustomerDetails;
import com.emidhealth.insurance.HealthInsurance.dao.PremiumDao;
import com.emidhealth.insurance.HealthInsurance.dto.CustomerHealthHabitDto;

public class PremiumServiceImpl implements PremiumService {

	
	@Autowired 
	PremiumDao dao;
	
	@Transactional
	public Float getPremiumAmmount(CustomerDetails customerDetails ) {
		return dao.getCustomerPremiumDetails(customerDetails);
	}

}
