package com.emidhealth.insurance.HealthInsurance.service;

import com.emidhealth.insurance.HealthInsurance.Entity.CustomerDetails;
import com.emidhealth.insurance.HealthInsurance.dto.CustomerHealthHabitDto;

public interface PremiumService { 
	
	public Float getPremiumAmmount(CustomerDetails customerDetails );

}
