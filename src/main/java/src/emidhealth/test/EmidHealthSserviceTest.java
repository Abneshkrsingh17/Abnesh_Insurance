package src.emidhealth.test;

import static org.mockito.Mockito.when;

import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.emidhealth.insurance.HealthInsurance.Entity.CustomerDetails;
import com.emidhealth.insurance.HealthInsurance.dao.PremiumDao;
import com.emidhealth.insurance.HealthInsurance.service.PremiumService;

public class EmidHealthSserviceTest {

	@Mock
	private PremiumService service;
	
	@Mock
	private PremiumDao dao;
	
	@Before
	public void setUp() { 
		MockitoAnnotations.initMocks(this);
		}
	
	@DataProvider
	private final CustomerDetails serviceData() {
		CustomerDetails cd=new CustomerDetails();
		
		cd.setAge(34);
		cd.setGender("M");
		cd.setAlcohal("Y");
		cd.setBloodPressure("N");
		cd.setBloodSugar("N");
		cd.setHypertension("N");
		cd.setDrugs("N");
		cd.setSmoking("N");
		cd.setOverWeight("N");
		
		return cd;
		
	}

	@Test(dataProvider="serviceData")
	public void premiumAmountTest() {
		
		when(dao.getCustomerPremiumDetails(serviceData())).thenReturn((float) 6650.00);
		service.getPremiumAmmount(serviceData());
	}
	
	
}
